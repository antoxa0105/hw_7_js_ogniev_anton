// DOM это структура документа для возможности получения доступа к любому объекту в нем


const listItems = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];  
const listParent = document.body;

function createList(arr, parent) {
  parent.insertAdjacentHTML('beforeend', `<ul>${arr.map(function(item){
    return `<li>${item}</li>`}).join("")}</ul>`);
};

createList(listItems, listParent);

